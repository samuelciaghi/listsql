
import axios from 'axios';

const elementoDaAggiungere = ({ elementoSingolo, completato, da_fare, datiDB, setDatiDB }) => {



    const eliminaElemento = () => {

        axios.delete('http://localhost:3000/elimina_task/' + elementoSingolo.id)
            .then(() => {
                console.log('Dato Eliminato: ' + JSON.stringify(elementoSingolo));

                setDatiDB(datiDB.filter(elemento => elemento.id !== elementoSingolo.id));
            });
    }

    const elementoCompletato = () => {

        axios.put('http://localhost:3000/task_completato/' + elementoSingolo.id, { completato: 1 - elementoSingolo.completato })
            .then(() => {
                //let elementoDaEliminare = datiDB.filter(elementoDaEliminare => elementoDaEliminare.id === elementoSingolo.id);
                console.log('Elemento modificato: ' + JSON.stringify(elementoSingolo));

                //Aggiorna il DOM dopo che viene cambiato il valore completato da 1 a 0
                setDatiDB(datiDB
                    .map((elementoModificato) => {
                        if (elementoModificato.id === elementoSingolo.id) {
                            //Ritorna l'elemento cliccato cambiando tra 0 e 1 lo stato del completamento
                            return { ...elementoModificato, completato: 1 - elementoModificato.completato }
                        }
                        console.log('Elemento modificato2: ' + JSON.stringify(elementoModificato));
                        return elementoModificato;
                    }))
            });
    }

    return (
        <div className="elemento">
            <li className={"stileLista " + (completato ? 'completato' : '')}><span>{elementoSingolo.id}</span>{da_fare}</li>
            <button onClick={elementoCompletato} className="btnCompletato">V</button>
            <button onClick={eliminaElemento} className="btnElimina">X</button>
        </div>
    );

}


export default elementoDaAggiungere;