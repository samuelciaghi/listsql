import ElementoDaAggiungere from "./gestioneElemento";
import axios from "axios";
import Select from "react-select";

const HTML = ({
  inputTesto,
  setValoreInput,
  setStato,
  filtro,
  datiDB,
  setDatiDB,
}) => {
  const options = [
    { value: "tutti", label: "tutti" },
    { value: "completato", label: "completato" },
    { value: "da-completare", label: "da-completare" },
  ];

  const stileSelect = {
  
  
    
  }

  const gestisciInput = (e) => {
    setValoreInput(e.target.value);
  };

  const aggiungiElemento = (e) => {
    e.preventDefault();

    //Prende l'ultimo elemento dell'array per ricavarne l'id
    //e aggiungendo +1 avere sempre una key univoca
    const [ultimoElemento] = datiDB.slice(-1);

    const taskDaAggiungere = {
      id: ultimoElemento.id + 1,
      da_fare: inputTesto,
      completato: 0,
    };
    axios
      .post("http://localhost:3000/aggiungi_da_fare/", taskDaAggiungere)
      .then(() => {
        console.log("Task Aggiunto:" + JSON.stringify(taskDaAggiungere));
        setDatiDB([...datiDB, taskDaAggiungere]);
      });

    setValoreInput("");
  };

  const filtroElementi = (e) => {
    setStato(e.value);
  };

  return (
    <div className="container">
      <div className="input">
        <h2>LISTA</h2>
        <div className="elemento">
          <input
            value={inputTesto}
            onChange={gestisciInput}
            id="inputTask"
            type="text"
            placeholder="Aggiungi Task alla Lista"
            autoComplete="off"
          />
          <div  className = "stileSelect">
          <Select
            onChange={filtroElementi}
            options={options}
            styles={stileSelect}
           
          />
          </div>
        </div>

        <button onClick={aggiungiElemento} id="bottoneTask">
          AGGIUNGI
        </button>
      </div>

      <div className="lista">
        <h2>DA FARE</h2>
        <ul className="stileLista" id="listaTask">
          {filtro.map((elementoSingolo) => (
            <ElementoDaAggiungere
              completato={elementoSingolo.completato}
              key={elementoSingolo.id} //necessaria per identificare ogni elemento che aggiungo
              elementoSingolo={elementoSingolo} //per passare ogni cosa da fare (elementoSingolo) in gestioneElemento.js
              datiDB={datiDB}
              setDatiDB={setDatiDB}
              da_fare={elementoSingolo.da_fare}
            />
          ))}
        </ul>
      </div>
    </div>
  );
};

export default HTML;
