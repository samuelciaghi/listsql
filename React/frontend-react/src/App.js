import "./App.css";
import HTML from "./visualizzaToDo/html";
//import ElementoDaAggiungere from "./visualizzaToDo/gestioneElemento";
import { useState, useEffect } from "react";
import axios from "axios";

function App() {
  const [inputTesto, setValoreInput] = useState("");
  const [coseDaFare, setCoseDaFare] = useState([]);
  const [stato, setStato] = useState("tutti");
  const [filtro, setFiltro] = useState([]);
  const [datiDB, setDatiDB] = useState([]);

  useEffect(() => {
    switch (stato) {
      case "completato":
        setFiltro(datiDB.filter((el) => el.completato !== 0));
        break;
      case "da-completare":
        setFiltro(datiDB.filter((el) => el.completato === 0));
        break;
      default:
        setFiltro(datiDB);
        break;
    }
  }, [datiDB,stato]);

  //Fetch api da Node.js appena carica la pagina
  useEffect(() => {

    axios.get('http://localhost:3000/lista').then(datiRicevuti => {
          console.log(datiRicevuti.data);
         setDatiDB(datiRicevuti.data)  ;
        });   
  },[]);


  return (
    <div className="App">
      <br />
      <HTML
        coseDaFare={coseDaFare}
        setCoseDaFare={setCoseDaFare}
        setValoreInput={setValoreInput}
        inputTesto={inputTesto}
        setStato={setStato}
        filtro={filtro}
        datiDB = {datiDB}
        setDatiDB = {setDatiDB}
      />

      {/*  <ElementoDaAggiungere 
      coseDaFare = {coseDaFare} 
      setCoseDaFare = { setCoseDaFare} /> */}
    </div>
  );
}

export default App;
