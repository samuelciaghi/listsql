
let mysql = require('mysql');

const configurazioneDB = {
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'listasql',
    port: 3306
}

let connessioneDB = {

    query: (querySQL, parametri, risposta) => {

        let connessione = mysql.createConnection(configurazioneDB);

        connessione.query(querySQL, parametri, (errore, datiDB) => {

            if (!errore) {

                console.log(datiDB);
                return risposta.json(datiDB);
            } else {

                console.log(errore);
                return risposta.json(errore);
            }
        })
    }
}

module.exports = connessioneDB;