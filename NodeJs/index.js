const express = require('express');
let mysql = require('mysql');
let cors = require('cors');
const app = express();
let connessioneDB = require('./connessioneDB.js');

app.use(cors({ credentials: true, origin: true }));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.get('/', (req, res) => {
    res.send('OK');
});

app.get('/lista', (req, res) => {

    connessioneDB.query('SELECT * FROM listaCose', null, res);

});

app.get('/lista/:id', (req, res) => {

    idElemento = req.params.id;
    connessioneDB.query('SELECT * FROM listaCose WHERE id = ?', idElemento, res);
    
});

app.post('/aggiungi_da_fare', (req,res) => {

    let da_fare = {
        id: req.body.id,
        da_fare: req.body.da_fare,
        completato: req.body.completato
    };

    connessioneDB.query('INSERT INTO listaCose SET ?', da_fare, res);
});

app.put('/task_completato/:id', (req,res) => {

    let idElemento = req.params.id;

    let completato = req.body.completato;
 
    connessioneDB.query('UPDATE listaCose SET completato = ' + completato + " WHERE id = " + idElemento,null, res);

});

app.delete('/elimina_task/:id', (req,res) => {

    let id = req.params.id;

    connessioneDB.query('DELETE FROM listaCose WHERE id = ?', id, res);

})



app.listen(3000, () => {
    console.log('PORTA 3000');
});