
# Progetto listaSQL

Applicazione ToDoList realizzata con React, Node.js e MySQL.

Ogni task inserito nella lista delle cose da fare viene automaticamente aggiunto nel database e tutte le modifiche effettuate (task completato, eliminazione task...) vengono salvate nel database tramite le operzioni CRUD previste in Nodejs.

# Avvio App

Aprire il file listaSQL.sql in MySQL per creare il Database.
Apire il CMD o un terminale:
Nella cartella NodeJs eseguire il comando `npm run dev` per avviare il server.
Nella cartella React/frontend-react eseguire il comando `npm start` per visualizzare il sito a `http://localhost:3006/`.


